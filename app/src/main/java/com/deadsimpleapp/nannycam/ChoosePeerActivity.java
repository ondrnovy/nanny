package com.deadsimpleapp.nannycam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;


public class ChoosePeerActivity extends AppCompatActivity {
    final static String APP_TAG = "NANNYLOG ChoosePeer| ";


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Log.d(APP_TAG, "onCreate");

        setContentView(R.layout.activity_choose_peer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        Intent intent = getIntent();
        String peersStr = intent.getStringExtra("peers");
        List<String> peers = Arrays.asList(peersStr.split(";"));


        LazyAdapter adapter = new LazyAdapter((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE), peers);



        ListView listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(APP_TAG, "Chosen " + position);

                Intent intent = new Intent();
                intent.putExtra("selected", position);
                setResult(Activity.RESULT_OK, intent);
                finish();

            }

        });
    }


}