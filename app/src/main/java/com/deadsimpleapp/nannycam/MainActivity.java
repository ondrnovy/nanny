package com.deadsimpleapp.nannycam;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;


import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;




/**
 * Created by deadsimpleapps on 29.9.2017.
 */

public class MainActivity extends Activity {


    final static String APP_TAG = "NANNYLOG Main| ";
    private boolean mIsBroadcaster = false;



    //region Permissions




    final int AUDIO_VIDEO_PERMISSIONS_REQUEST_CODE = 489;



    public boolean hasAudioVideoPermission(){

        int permissionVideo = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int permissionAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        return permissionVideo == PackageManager.PERMISSION_GRANTED && permissionAudio == PackageManager.PERMISSION_GRANTED;
    }


    public void needAudioVideoPermission(){

        if(!hasAudioVideoPermission()){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    AUDIO_VIDEO_PERMISSIONS_REQUEST_CODE);
        }

    }





    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case AUDIO_VIDEO_PERMISSIONS_REQUEST_CODE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {


                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


        }
    }


    //endregion Permissions







    //region Volume

    Integer mPrevMediaVolume = null;

    private void maximizeMediaVolume(){

        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        mPrevMediaVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        for(int i = 0; i < 100; i++){
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
        }
    }

    private void unmaximizeMediaVolume(){
       if(mPrevMediaVolume != null){
           AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
           audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mPrevMediaVolume, 0);
       }

    }



    Integer mPrevVoiceCallVolume = null;

    private void maximizeVoiceCallVolume(){

        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        mPrevVoiceCallVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);

        for(int i = 0; i < 100; i++){
            audioManager.adjustStreamVolume(AudioManager.STREAM_VOICE_CALL, AudioManager.ADJUST_RAISE, 0);
        }
    }

    private void unmaximizeVoiceCallVolume(){
        if(mPrevVoiceCallVolume != null){
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mPrevVoiceCallVolume, 0);
        }

    }






    Integer[] mOriginalAudioStreamVolumes = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };

    private void muteAudioStream(int audioStream, boolean on, AudioManager audioManager){

        Integer prevVolume = audioManager.getStreamVolume(audioStream);

        audioManager.setStreamMute(audioStream, on);

        if(on && prevVolume > 0 && prevVolume == audioManager.getStreamVolume(audioStream)){
            mOriginalAudioStreamVolumes[audioStream] = prevVolume;

            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
            audioManager.adjustStreamVolume(audioStream, AudioManager.ADJUST_LOWER, 0);
        }

        if(!on &&  mOriginalAudioStreamVolumes[audioStream] != null) {
            audioManager.setStreamVolume(audioStream, mOriginalAudioStreamVolumes[audioStream], 0);
        }
    }


    Integer mOriginalRingerMode = null;

    private void silentMode(boolean on){

        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        int prevRingVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        int prevAlarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        int prevCallVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
        int prevMusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int prevNotificationVolume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        int prevSystemVolume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        int prevDtmfVolume = audioManager.getStreamVolume(AudioManager.STREAM_DTMF);


        muteAudioStream(AudioManager.STREAM_RING, on, audioManager);
        muteAudioStream(AudioManager.STREAM_ALARM, on, audioManager);
        muteAudioStream(AudioManager.STREAM_VOICE_CALL, on, audioManager);
        muteAudioStream(AudioManager.STREAM_MUSIC, on, audioManager);
        muteAudioStream(AudioManager.STREAM_NOTIFICATION, on, audioManager);
        muteAudioStream(AudioManager.STREAM_SYSTEM, on, audioManager);
        muteAudioStream(AudioManager.STREAM_DTMF, on, audioManager);


        Log.i(APP_TAG, "Ring volume set from " + prevRingVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_RING));
        Log.i(APP_TAG, "Alarm volume set from " + prevAlarmVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_ALARM));
        Log.i(APP_TAG, "Call volume set from " + prevCallVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL));
        Log.i(APP_TAG, "Music volume set from " + prevMusicVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        Log.i(APP_TAG, "Notification volume set from " + prevNotificationVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
        Log.i(APP_TAG, "System volume set from " + prevSystemVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM));
        Log.i(APP_TAG, "DTMF volume set from " + prevDtmfVolume + " to " + audioManager.getStreamVolume(AudioManager.STREAM_DTMF));


        if(on) {
            mOriginalRingerMode = audioManager.getRingerMode();
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        }
        else{
            if(mOriginalRingerMode != null) {
                audioManager.setRingerMode(mOriginalRingerMode);
            }
        }
    }


    private void setSilentMode() {
        try {
            Log.i(APP_TAG, "turning on silent mode");
            silentMode(true);
        } catch (Exception e) {
            Log.e(APP_TAG, "Cannot set silent mode: " + e.getMessage());
        }
    }

    private void cancelSilentMode() {
        try {
            Log.i(APP_TAG, "turning off silent mode");
            silentMode(false);
        } catch (Exception e) {
            Log.e(APP_TAG, "Cannot cancel silent mode: " + e.getMessage());
        }
    }


    //endregion Volume





    //region Battery

    private BroadcastReceiver mBatteryInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

            Log.i(APP_TAG, "Battery changed to " + level + "%");


            //sendToPeer("battery status", Integer.toString(level));
        }
    };

    private void initBatteryStatus(){
        registerReceiver(mBatteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    //endregion Battery






    //region Wake Lock

    private PowerManager.WakeLock mWakeLock = null;

    private void preventAppBeTurnedOff(){
        Log.i(APP_TAG, "Preventing app from being turned off");

        try {
            if (mWakeLock == null) {
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "NannyWakelock");
                mWakeLock.acquire();
            }
        }
        catch(Exception e){
            Log.e(APP_TAG, "Preventing app from being turned off failed");
        }
    }

    private void allowAppBeTurnedOff(){
        Log.i(APP_TAG, "Allowing app to be turned off");

        try {
            if (mWakeLock != null) {
                mWakeLock.release();
            }
        }
        catch(Exception e){
            Log.e(APP_TAG, "Allowing app to be turned off failed");
        }

        mWakeLock = null;
    }

    //endregion Wake Lock





    //region Roles


    private void initBroadcaster() {

        mIsBroadcaster = true;

        setSilentMode();
        //initBatteryStatus();
    }

    private void initReceiver() {

        mIsBroadcaster = false;

        //maximizeMediaVolume();
        maximizeVoiceCallVolume();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    //endregion Roles





    //region WebRTC

    private PeerConnectionFactory mPeerConnectionFactory = null;

    private VideoTrack mLocalVideoTrack = null;
    private VideoSource mLocalVideoSource = null;

    private AudioSource mLocalAudioSource = null;
    private AudioTrack mLocalAudioTrack = null;

    private MediaStream mLocalMediaStream = null;
    private String mLocalMediaStreamId = null;
    private MediaStream mRemoteMediaStream = null;

    private PeerConnection mPeerConnection = null;

    private VideoRenderer mVideoRenderer = null;

    VideoCapturerAndroid mVideoCapturer = null;


    private static String getRandomId() {
        return UUID.randomUUID().toString();
    }


    private final String BACK_FACING_CAMERA = "Camera 0, Facing back, Orientation 90";
    private final String FRONT_FACING_CAMERA = "Camera 1, Facing front, Orientation 270";

    private boolean mIsBackFacingCameraSet = true;



    private VideoCapturer getVideoCapturer() {
        if (mVideoCapturer == null) {

            VideoCapturerAndroid.CameraEventsHandler cameraEventsHandler = new VideoCapturerAndroid.CameraEventsHandler() {
                @Override
                public void onCameraError(String var1) {
                    Log.e(APP_TAG, "onCameraError: " + var1);
                }

                @Override
                public void onCameraFreezed(String var1) {
                    Log.d(APP_TAG, "onCameraFreezed: " + var1);
                }

                @Override
                public void onCameraOpening(int var1) {
                    Log.d(APP_TAG, "onCameraOpening: " + var1);
                }


                @Override
                public void onFirstFrameAvailable() {
                    Log.d(APP_TAG, "onFirstFrameAvailable");
                }

                @Override
                public void onCameraClosed() {
                    Log.d(APP_TAG, "onCameraClosed");
                }
            };



            mVideoCapturer = VideoCapturerAndroid.create(mIsBackFacingCameraSet ? BACK_FACING_CAMERA : FRONT_FACING_CAMERA, cameraEventsHandler);
        }

        return mVideoCapturer;
    }


    private AudioTrack getLocalAudioTrack() {

        if (mLocalAudioTrack == null) {
            MediaConstraints audioConstraints = getMediaConstraints();

            final String AUDIO_TRACK_ID = getRandomId();
            mLocalAudioSource = mPeerConnectionFactory.createAudioSource(audioConstraints);
            mLocalAudioTrack = mPeerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, mLocalAudioSource);
        }

        return mLocalAudioTrack;
    }


    private VideoTrack getLocalVideoTrack() {

        if (mLocalVideoTrack == null) {

            MediaConstraints videoConstraints = getMediaConstraints();

            final String VIDEO_TRACK_ID = getRandomId();
            mLocalVideoSource = mPeerConnectionFactory.createVideoSource(getVideoCapturer(), videoConstraints);
            mLocalVideoTrack = mPeerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, mLocalVideoSource);
        }

        return mLocalVideoTrack;
    }


    private String getLocalMediaStreamId() {
        if (mLocalMediaStreamId == null) {
            mLocalMediaStreamId = getRandomId();
        }

        return mLocalMediaStreamId;
    }

    private MediaStream getLocalMediaStream(boolean includeAudio) {

        if (mLocalMediaStream == null) {
            mLocalMediaStream = mPeerConnectionFactory.createLocalMediaStream(getLocalMediaStreamId());

            mLocalMediaStream.addTrack(getLocalVideoTrack());
            if (includeAudio) {
                mLocalMediaStream.addTrack(getLocalAudioTrack());
            }
        }

        return mLocalMediaStream;
    }


    private boolean isMultipleCamerasAvailable() {
        return true;
        //return VideoCapturerAndroid.getDeviceCount() > 1;
    }






    private PeerConnection getPeerConnection() {

        if (mPeerConnection == null) {
            mPeerConnection = mPeerConnectionFactory.createPeerConnection(getIceServers(), getMediaConstraints(), getObserver());
        }

        return mPeerConnection;
    }

    private void destroyPeerConnectionAndReleaseResources(){

        if(mLocalVideoSource != null){
            mLocalVideoSource.stop();
        }

        if(mPeerConnection != null) {
            mPeerConnection.dispose();
        }

        mPeerConnection = null;
        mRemoteMediaStream = null;
        mLocalAudioTrack = null;
        mLocalVideoTrack = null;
        mLocalAudioSource = null;
        mLocalVideoSource = null;
        mLocalMediaStream = null;
        mLocalMediaStreamId = null;
        if(mVideoCapturer != null) {
            mVideoCapturer.dispose();
            mVideoCapturer = null;
        }
    }










    private void startAudioVideo() {

        try {
            initWebRtcOnThisDevice();
        } catch (Exception e) {
            Log.e(APP_TAG, "Initializing WebRTC FAILED " + e.getMessage());
        }

        if(mIsBroadcaster){
            MediaStream mediaStream = getLocalMediaStream(/*include audio*/true);
            showMediaStreamVideo(mediaStream);
            getPeerConnection().addStream(mediaStream);

            Log.d(APP_TAG, "BROADCASTER - Create offer");
            getPeerConnection().createOffer(getSdpObserverForOfferOrAnswer(/*offer*/true), getMediaConstraints());
        }
    }



    private void showMediaStreamVideo(MediaStream mediaStream) {


        if(!mIsBroadcaster){
            mVideoRenderer = null;
        }

        VideoTrack track = mediaStream.videoTracks.get(0);


        try {
            if (mVideoRenderer == null) {
                mVideoRenderer = VideoRendererGui.createGui(0, 0, 100, 100, RendererCommon.ScalingType.SCALE_ASPECT_FILL, false);
            }

            track.addRenderer(mVideoRenderer);
        } catch (Exception e) {
            Log.e(APP_TAG, "showMediaStreamVideo FAILED");
        }
    }


    private void initRenderer() throws Exception {
        GLSurfaceView videoView = (GLSurfaceView) findViewById(R.id.surface_view);


        VideoRendererGui.setView(videoView, new Runnable() {
            @Override
            public void run() {
                Log.v(APP_TAG, "eglContextReadyCallback");
            }
        });
    }


    private void initWebRtcOnThisDevice() throws Exception {
        if (!PeerConnectionFactory.initializeAndroidGlobals(this, true, true, true)) {
            Log.e(APP_TAG, "PeerConnectionFactory.initializeAndroidGlobals FAILED");
        }

        mPeerConnectionFactory = new PeerConnectionFactory();

        initRenderer();
    }


    private List<PeerConnection.IceServer> getIceServers() {
        LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
        iceServers.add(new PeerConnection.IceServer("stun:stun.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun1.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun2.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun3.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun4.l.google.com:19302"));

        return iceServers;
    }


    private static MediaConstraints getMediaConstraints() {
        MediaConstraints mediaConstraints = new MediaConstraints();
        mediaConstraints.optional.add(new MediaConstraints.KeyValuePair("googCpuOveruseDetection", "false"));

        /*mediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("minWidth", "240"));
        mediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("minHeight", "300"));
        mediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("minFrameRate", "30"));*/
        return mediaConstraints;
    }


    private PeerConnection.Observer getObserver() {

        return new PeerConnection.Observer() {

            @Override
            public void onDataChannel(DataChannel dataChannel) {
                Log.d(APP_TAG, "onDataChannel");
            }

            @Override
            public void onAddStream(MediaStream mediaStream) {
                Log.d(APP_TAG, "onAddStream - label: " + mediaStream.label() + ", local label: " + getLocalMediaStreamId());


                if (!mIsBroadcaster) {
                    mRemoteMediaStream = mediaStream;
                    showMediaStreamVideo(mRemoteMediaStream);
                }
            }

            @Override
            public void onRemoveStream(MediaStream mediaStream) {
                Log.d(APP_TAG, "onRemoveStream" + mediaStream.label());

            }

            @Override
            public void onSignalingChange(PeerConnection.SignalingState signalingState) {
                Log.d(APP_TAG, "onSignalingChange - " + signalingState.name());
            }

            @Override
            public void onIceConnectionReceivingChange(boolean var1) {
                Log.d(APP_TAG, "onIceConnectionReceivingChange - " + var1);

            }

            @Override
            public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
                Log.d(APP_TAG, "onIceConnectionChange - " + iceConnectionState.name());


                if(iceConnectionState == PeerConnection.IceConnectionState.COMPLETED || iceConnectionState == PeerConnection.IceConnectionState.CONNECTED){
                    setWatching(true);
                }
                else if(iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED){
                    setWatching(false);
                }
                else if(iceConnectionState == PeerConnection.IceConnectionState.FAILED){
                    mIsAudioVideoRunning = false;
                }
            }


            @Override
            public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                Log.d(APP_TAG, "onIceGatheringChange - " + iceGatheringState.name());
            }

            @Override
            public void onIceCandidate(IceCandidate iceCandidate) {
                Log.d(APP_TAG, "onIceCandidate - " + iceCandidate.toString());

                if (mIsBroadcaster) {
                    Log.d(APP_TAG, "BROADCASTER - Sending ice candidates - " + iceCandidate.toString());
                    //sendToPeer("ice candidates", iceCandidateToJson(iceCandidate));
                    sendIceCandidatesToPeer(iceCandidate);
                }
            }


            @Override
            public void onRenegotiationNeeded() {
                Log.d(APP_TAG, "onRenegotiationNeeded");
            }


        };
    }


    private SdpObserver getSdpObserverForSetDescription(final boolean remote) {

        return new SdpObserver() {
            @Override
            /** Called on success of Create{Offer,Answer}(). */
            public void onCreateSuccess(SessionDescription sessionDescription) {
                Log.v(APP_TAG, "onCreateSuccess");
            }

            @Override
            /** Called on success of Set{Local,Remote}Description(). */
            public void onSetSuccess() {
                Log.v(APP_TAG, "onSetSuccess");

                if (!mIsBroadcaster) {
                    if (remote) {
                        Log.d(APP_TAG, "RECEIVER - Create answer");
                        getPeerConnection().createAnswer(getSdpObserverForOfferOrAnswer(/*answer*/false), getMediaConstraints());
                    }
                }
            }

            @Override
            /** Called on error of Create{Offer,Answer}(). */
            public void onCreateFailure(String s) {
                Log.v(APP_TAG, "onCreateFailure " + s);
            }

            @Override
            /** Called on error of Set{Local,Remote}Description(). */
            public void onSetFailure(String s) {
                Log.e(APP_TAG, "onSetFailure " + s);
            }
        };
    }


    private static JSONObject sessionDescriptionToJson(SessionDescription sessionDescription) {

        try {
            JSONObject json = new JSONObject();
            json.put("type", sessionDescription.type.canonicalForm());
            json.put("sdp", sessionDescription.description);

            return json;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    private static SessionDescription jsonToSessionDescription(JSONObject json) {
        try {
            String type = json.get("type").toString();
            String sdp = json.get("sdp").toString();

            return new SessionDescription(SessionDescription.Type.fromCanonicalForm(type), sdp);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    private static JSONObject iceCandidateToJson(IceCandidate iceCandidate) {
        try {
            JSONObject json = new JSONObject();
            json.put("sdpMid", iceCandidate.sdpMid);
            json.put("sdpMLineIndex", Integer.toString(iceCandidate.sdpMLineIndex));
            json.put("sdp", iceCandidate.sdp);


            return json;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private static IceCandidate jsonToIceCandidate(JSONObject json) {
        try {
            String sdpMid = json.get("sdpMid").toString();
            int sdpMLineIndex = Integer.parseInt(json.get("sdpMLineIndex").toString());
            String sdp = json.get("sdp").toString();

            return new IceCandidate(sdpMid, sdpMLineIndex, sdp);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    private SdpObserver getSdpObserverForOfferOrAnswer(final boolean offer) {

        return new SdpObserver() {

            @Override
            /** Called on success of Create{Offer,Answer}(). */
            public void onCreateSuccess(SessionDescription sessionDescription) {
                Log.v(APP_TAG, "onCreateSuccess");


                if (mIsBroadcaster) {
                    assert offer;
                    Log.d(APP_TAG, "BROADCASTER - Set local description - " + sessionDescription.toString());
                    getPeerConnection().setLocalDescription(getSdpObserverForSetDescription(/*local*/ false), sessionDescription);


                    Log.d(APP_TAG, "BROADCASTER - Sending offer - " + sessionDescription.toString());
                    //sendToPeer("description", sessionDescriptionToJson(sessionDescription));
                    sendDescriptionToPeer(sessionDescription);

                } else {
                    assert !offer;
                    Log.d(APP_TAG, "RECEIVER - Set local description - " + sessionDescription.toString());
                    getPeerConnection().setLocalDescription(getSdpObserverForSetDescription(/*local*/ false), sessionDescription);

                    Log.d(APP_TAG, "RECEIVER - Sending answer - " + sessionDescription.toString());
                    //sendToPeer("description", sessionDescriptionToJson(sessionDescription));
                    sendDescriptionToPeer(sessionDescription);

                }


            }

            @Override
            /** Called on success of Set{Local,Remote}Description(). */
            public void onSetSuccess() {
                Log.v(APP_TAG, "onSetSuccess");
            }

            @Override
            /** Called on error of Create{Offer,Answer}(). */
            public void onCreateFailure(String s) {
                Log.v(APP_TAG,"onCreateFailure " + s);
            }

            @Override
            /** Called on error of Set{Local,Remote}Description(). */
            public void onSetFailure(String s) {
                Log.v(APP_TAG, "onSetFailure " + s);
            }
        };
    }


    public void switchCamera() {

        try {
            VideoCapturerAndroid.CameraSwitchHandler cameraSwitchHandler = new VideoCapturerAndroid.CameraSwitchHandler() {
                @Override
                public void onCameraSwitchDone(boolean isFrontCamera){
                    Log.d(APP_TAG, "onCameraSwitchDone: " + isFrontCamera);
                    mIsBackFacingCameraSet = !isFrontCamera;
                }

                @Override
                public void onCameraSwitchError(String errorDescription){
                    Log.e(APP_TAG, "onCameraSwitchError: " + errorDescription);
                }



            };
            mVideoCapturer.switchCamera(cameraSwitchHandler);

        } catch (Exception e) {
            Log.e(APP_TAG, "Cannot switch camera: " + e.getMessage());
        }
    }









    private void videoTurned(final boolean on){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {



                getTurnVideoButtton().setEnabled(true);
                getLoading().setVisibility(View.INVISIBLE);

                if(on){
                    getVideoTurnedOffImage().setVisibility(View.INVISIBLE);
                    getTurnVideoButtton().setImageResource(R.mipmap.ic_video_off_button2);
                }
                else{
                    getVideoTurnedOffImage().setVisibility(View.VISIBLE);
                    getTurnVideoButtton().setImageResource(R.mipmap.ic_video_on_button2);
                }

            }
        });
    }

    private void turnVideo(final boolean on) {

        if(mIsBroadcaster){
            if (on) {
                mLocalVideoSource.restart();
            } else {
                mLocalVideoSource.stop();
            }

            sendVideoTurnedToPeer(on);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    getSwitchCameraButtton().setEnabled(on);

                    if(on){
                        getVideoTurnedOffImage().setVisibility(View.INVISIBLE);
                    }
                    else{
                        getVideoTurnedOffImage().setVisibility(View.VISIBLE);
                    }

                }
            });
        }
        else{
            sendTurnVideoToPeer(on);
        }



    }




    //endregion WebRTC





    //region Transfer Layer Interface

    void sendDescriptionToPeer(SessionDescription offer){
        Log.d(APP_TAG, "TRANSFERING... description");
        sendAsyncMessageToPeer("D" + sessionDescriptionToJson(offer).toString());
    }

    void sendIceCandidatesToPeer(IceCandidate iceCandidates){
        Log.d(APP_TAG, "TRANSFERING... ice candidates");
        sendAsyncMessageToPeer("I" + iceCandidateToJson(iceCandidates).toString());
    }


    void sendTurnVideoToPeer(boolean on){
        Log.d(APP_TAG, "TRANSFERING... turn video");
        sendAsyncMessageToPeer("T" + (on ? "1" : "0"));
    }

    void sendVideoTurnedToPeer(boolean on){
        Log.d(APP_TAG, "TRANSFERING... video turned");
        sendAsyncMessageToPeer("V" + (on ? "1" : "0"));
    }



    void processMessageFromPeer(String message){
        Log.i(APP_TAG, "PICKED " + message + " from peer");

        if(message.isEmpty()){
            return;
        }

        String messageType = message.substring(0,1);
        String messageBody = message.substring(1);

        if(messageType.equals("D")){

            try {
                JSONObject json = new JSONObject(messageBody);
                SessionDescription sessionDescription = jsonToSessionDescription(json);
                Log.d(APP_TAG, "BROADCASTER - Set remote description");
                if(!mIsBroadcaster){
                    destroyPeerConnectionAndReleaseResources();
                }
                getPeerConnection().setRemoteDescription(getSdpObserverForSetDescription(/*remote*/ true), sessionDescription);
            }
            catch(JSONException e){
                Log.e(APP_TAG, "Cannot convert description to json " + e.getMessage());
            }

        }
        else if(messageType.equals("I")){

            try {
                JSONObject json = new JSONObject(messageBody);
                IceCandidate remoteIceCandidate = jsonToIceCandidate(json);
                Log.d(APP_TAG, "RECEIVER - Receive ice candidates - " + remoteIceCandidate.toString());
                getPeerConnection().addIceCandidate(remoteIceCandidate);
            }
            catch(JSONException e){
                Log.e(APP_TAG, "Cannot convert ice candidate to json " + e.getMessage());
            }

        }
        else if(messageType.equals("T")){
            turnVideo(messageBody.equals("1") ? true : false);
        }
        else if(messageType.equals("V")){
            videoTurned(messageBody.equals("1") ? true : false);
        }
        else {
            Log.w(APP_TAG, "Unhandled message type: " + message);
        }

    }



    //endregion Transfer Layer Interface




    //region Transfer Layer


    void sendAsyncMessageToPeer(String message){
        Log.i(APP_TAG, "PUSHED " + message + " to peer");
        mOutcomingMessageQueue.add(message);
    }

    void processAsyncMessagesFromPeer(){

        while(!mIncomingMessageQueue.isEmpty()){
            String message = (String) mIncomingMessageQueue.remove();
            processMessageFromPeer(message);
        }
    }

    Queue mIncomingMessageQueue = new LinkedList();
    Queue mOutcomingMessageQueue = new LinkedList();




    final Handler mHandler = new Handler();

    private void launchProcessAsyncMessageTaskTimer() {
        mHandler.postDelayed(processAsyncMessageTask, 500);
    }


    final Runnable processAsyncMessageTask = new Runnable() {

        public void run() {
            processAsyncMessagesFromPeer();
            mHandler.postDelayed(processAsyncMessageTask, 500);
        }

    };


    //endregion Transfer Layer









    private void launchAudioVideoTask() {
        mHandler.postDelayed(audioVideoTask, 500);
    }

    public boolean mIsAudioVideoRunning = false;

    final Runnable audioVideoTask = new Runnable() {

        public void run() {


        if(mIsSomehowConnected && hasAudioVideoPermission() && !mIsAudioVideoRunning){
            startAudioVideo();
            mIsAudioVideoRunning = true;
        }


        mHandler.postDelayed(audioVideoTask, 500);




        }

    };









    public boolean mIsSomehowConnected = false;



    //region WiFi

    public void turnWifi(boolean on){
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if(wifiManager.isWifiEnabled() != on){
            Toast.makeText(getApplicationContext(), R.string.message_turning_on_wifi, Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(on);
        }
    }

    //endregion WiFi



    //region WiFi P2P

    private final IntentFilter mIntentFilter = new IntentFilter();

    WifiP2pManager.Channel mChannel = null;
    WifiP2pManager mManager = null;

    void initWifiDirect(){
        // Indicates a change in the Wi-Fi P2P status.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);


        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
    }



    String p2pFailureReasonCodeToReason(int reasonCode){
        String reason = "";

        if(reasonCode==0){
            reason = "ERROR";
        }
        else if(reasonCode==1){
            reason = "P2P_UNSUPPORTED";
        }
        else if(reasonCode==2){
            reason = "BUSY";
        }

        return reason;
    }


    void discoverPeers(){


        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.d(APP_TAG, "discoverPeers - onSuccess");


                // Code for when the discovery initiation is successful goes here.
                // No services have actually been discovered yet, so this method
                // can often be left blank. Code for peer discovery goes in the
                // onReceive method, detailed below.
            }

            @Override
            public void onFailure(int reasonCode) {


                Log.d(APP_TAG, "discoverPeers - onFailure " + reasonCode + "=" + p2pFailureReasonCodeToReason(reasonCode));


                turnWifi(true);

                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        discoverPeers();
                    };
                }, 2000);


                // Code for when the discovery initiation fails goes here.
                // Alert the user that something went wrong.
            }
        });
    }


    boolean mPeersConnected = false;
    boolean mIsGroupOwner = false;
    String mGroupOwnerAddressHost = null;

    private BroadcastReceiver mWifiDirectReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            Log.d(APP_TAG, "WiFi Direct - onReceive " + action.toString());

            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                // Determine if Wifi P2P mode is enabled or not, alert
                // the Activity.
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);



                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    Log.i(APP_TAG, "WiFi P2P enabled");

                } else {
                    Log.i(APP_TAG, "WiFi P2P disabled");

                }
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

                // The peer list has changed! We should probably do something about
                // that.

                // Request available peers from the wifi p2p manager. This is an
                // asynchronous call and the calling activity is notified with a
                // callback on PeerListListener.onPeersAvailable()
                if (mManager != null) {
                    mManager.requestPeers(mChannel, peerListListener);
                }

            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

                // Connection state changed! We should probably do something about
                // that.

                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                Log.d(APP_TAG, "WIFI_P2P_CONNECTION_CHANGED_ACTION getParcelableExtra: " + networkInfo.toString());



                WifiP2pInfo p2pInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);


                if (networkInfo.isConnected()) {
                    Log.i(APP_TAG, "WiFi P2P connected");

                    mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener(){
                        @Override
                        public void onConnectionInfoAvailable(WifiP2pInfo info){
                            Log.d(APP_TAG, "onConnectionInfoAvailable " + info.toString());


                            // InetAddress from WifiP2pInfo struct.

                            // After the group negotiation, we can determine the group owner
                            // (server).

                            if (info.groupFormed){
                                boolean wasConnectedBefore = mPeersConnected;
                                mPeersConnected = true;
                                mIsGroupOwner = info.isGroupOwner;
                                mGroupOwnerAddressHost = info.groupOwnerAddress.getHostAddress();


                                mIsBroadcaster = mIsGroupOwner;

                                if(!wasConnectedBefore) {
                                    if (mIsBroadcaster) {
                                        initBroadcaster();
                                    } else {
                                        initReceiver();
                                    }
                                }

                                startWiFiDirectMessaging();
                            }
                            else{
                                Log.e(APP_TAG, "onConnectionInfoAvailable group not formed");
                            }


                        }

                    });


                } else {
                    Log.i(APP_TAG, "WiFi P2P disconnected");
                }


            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {


                WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                Log.d(APP_TAG, "WIFI_P2P_THIS_DEVICE_CHANGED_ACTION getParcelableExtra: " + device.toString());

            }
        }
    };



    boolean mPeerChosen = false;
    List<WifiP2pDevice> mPeers = null;

    private WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {
            Log.d(APP_TAG, "WifiP2pManager.PeerListListener - onPeersAvailable ");


            if(mPeerChosen || mPeersConnected){
                return;
            }

            mPeers = new ArrayList(peerList.getDeviceList());
            Log.d(APP_TAG, "PEERS: " + mPeers);




            if (mPeers.size() == 0) {
                Toast.makeText(getApplicationContext(), R.string.message_no_device_to_pair, Toast.LENGTH_LONG).show();
            }
            else if (mPeers.size() == 1) {
                mPeerChosen = true;
                connect(mPeers.get(0));
            }
            else{
                String deviceNames = "";
                for (WifiP2pDevice peer : mPeers) {
                    if(deviceNames.isEmpty()){
                        deviceNames += peer.deviceName;
                    }
                    else{
                        deviceNames += ";" + peer.deviceName;
                    }
                }

                Intent intent = new Intent(getApplicationContext(), ChoosePeerActivity.class);
                intent.putExtra("peers", deviceNames);
                startActivityForResult(intent, 666);
            }

        }
    };



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(APP_TAG, "onActivityResult " + requestCode + " " + resultCode + " " + data);


        if (requestCode == 666) {
            Integer position = data.getIntExtra("selected", -1);

            if(position > -1){
                Log.d(APP_TAG, "SELECTED " + position);
                mPeerChosen = true;
                connect(mPeers.get(position));
            }
        }

    }

    public void connect(WifiP2pDevice peer) {
        Log.i(APP_TAG, "Connecting to " + peer.toString());
        Toast.makeText(getApplicationContext(), getString(R.string.message_connecting_to) + " " + peer.deviceName, Toast.LENGTH_LONG).show();

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = peer.deviceAddress;
        config.wps.setup = WpsInfo.PBC;





        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.d(APP_TAG, "connect onSuccess");


                /*mManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener(){
                    @Override
                    public void onGroupInfoAvailable(WifiP2pGroup group){
                        Log.d(APP_TAG, "onGroupInfoAvailable " + group.toString());
                    }

                });*/
            }

            @Override
            public void onFailure(int reason) {
                Log.d(APP_TAG, "connect onFailure " + p2pFailureReasonCodeToReason(reason));
            }
        });
    }















    public class SendOrReceiveMessageAsyncTask extends AsyncTask<String, Integer, String> {

        boolean isGroupOwner;
        String address;



        public SendOrReceiveMessageAsyncTask(boolean isGroupOwner, String address) {
            this.isGroupOwner = isGroupOwner;
            this.address = address;
        }

        final int PORT = 8999;








        @Override
        protected String doInBackground(String... params) {


            Log.i(APP_TAG, "doInBackground... ");


            ObjectOutputStream outputStream;
            ObjectInputStream inputStream;

            ServerSocket serverSocket = null;
            Socket client = null;
            Socket socket = null;

            while(true){
                try{

                    if(isGroupOwner){
                        Log.i(APP_TAG, "serverSocket... ");

                        serverSocket = new ServerSocket(PORT);
                        client = serverSocket.accept();
                        outputStream  = new ObjectOutputStream(client.getOutputStream());
                        inputStream = new ObjectInputStream(client.getInputStream());
                    }
                    else{
                        Log.i(APP_TAG, "socket... ");
                        //Thread.sleep(5000);
                        socket = new Socket();
                        try {
                            socket.bind(null);
                            socket.connect((new InetSocketAddress(address, PORT)), 100000);
                            outputStream = new ObjectOutputStream(socket.getOutputStream());
                            inputStream = new ObjectInputStream(socket.getInputStream());
                        }
                        catch (Exception e){
                            socket.close();
                            Log.e(APP_TAG, "accept/connect error " + e.getMessage());
                            continue;
                        }
                    }


                }
                catch(Exception e){
                    Log.e(APP_TAG, "accept/connect error " + e.getMessage());
                    continue;
                }

                break;
            }


            Log.i(APP_TAG, "socket accepted/connected... ");




            while(true) {

                String messageToSend = "";
                if(!mOutcomingMessageQueue.isEmpty()){
                    messageToSend = (String) mOutcomingMessageQueue.remove();
                }


                try {
                    if(isGroupOwner) {
                        outputStream.writeUTF(messageToSend);
                        outputStream.flush();

                        String message = inputStream.readUTF();
                        if(!message.isEmpty()){
                            mIncomingMessageQueue.add(message);
                        }
                    }
                    else{
                        String message = inputStream.readUTF();
                        if(!message.isEmpty()){
                            mIncomingMessageQueue.add(message);
                        }

                        outputStream.writeUTF(messageToSend);
                        outputStream.flush();
                    }

                }
                catch(Exception e) {
                    Log.e(APP_TAG, "send/receive error " + e.getMessage());
                    Log.i(APP_TAG, "FUCK that socket ");

                    try {
                        if (isGroupOwner) {
                            client = serverSocket.accept();
                            outputStream = new ObjectOutputStream(client.getOutputStream());
                            inputStream = new ObjectInputStream(client.getInputStream());
                        }
                        else {
                            Thread.sleep(2000);
                            socket = new Socket();
                            socket.bind(null);
                            socket.connect((new InetSocketAddress(address, PORT)), 100000);
                            outputStream = new ObjectOutputStream(socket.getOutputStream());
                            inputStream = new ObjectInputStream(socket.getInputStream());
                        }
                    } catch (Exception e2) {
                        Log.e(APP_TAG, "accept/connect error " + e2.getMessage());
                    }
                }



                try {
                    Thread.sleep(100);
                }
                catch(Exception e){
                }

            }

        }

        @Override
        protected void onPostExecute(String result) {
        }
    }



    //endregion WiFi P2P






    //region Animations

    private Animation getFadeInAnimation() {
        return AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
    }

    private Animation getFadeOutAnimation() {
        return AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
    }

    private Animation getFadeOutQuickAnimation() {
        return AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_quick);
    }



    //endregion Animations



    //region GUI


    void turnOffSplashScreen(){
        getSplashScreenBackground().setVisibility(View.GONE);
        getLoading().setVisibility(View.GONE);
        getSplashScreenIcon().setVisibility(View.GONE);
    }





    public boolean mWasSetWatchingBefore = false;


    void setWatching(final boolean on){



        if(!mWasSetWatchingBefore) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    turnOffSplashScreen();
                    findViewById(R.id.camera_view).setVisibility(View.VISIBLE);

                    if(mIsBroadcaster){
                        try {
                            getBabyDeviceImage().setVisibility(View.VISIBLE);
                            getBabyDeviceImage().startAnimation(getFadeInAnimation());

                            ((ImageView)findViewById(R.id.role_big_icon)).setImageResource(R.drawable.ic_vector_baby);
                            findViewById(R.id.role_big_icon).startAnimation(getFadeOutAnimation());

                            ((TextView)findViewById(R.id.role_description)).setText(R.string.baby_device);
                            findViewById(R.id.role_description).startAnimation(getFadeOutAnimation());

                            Toast.makeText(getApplicationContext(), R.string.message_silent_mode, Toast.LENGTH_LONG).show();
                        }
                        catch(Exception e){
                            Log.e(APP_TAG, "Setting up broadcaster GUI FAILED");
                        }
                    }
                    else{
                        try{
                            getParentDeviceImage().setVisibility(View.VISIBLE);
                            getParentDeviceImage().startAnimation(getFadeInAnimation());


                            ((ImageView)findViewById(R.id.role_big_icon)).setImageResource(R.drawable.ic_vector_eye);
                            findViewById(R.id.role_big_icon).startAnimation(getFadeOutAnimation());

                            ((TextView)findViewById(R.id.role_description)).setText(R.string.parent_device);
                            findViewById(R.id.role_description).startAnimation(getFadeOutAnimation());

                            Toast.makeText(getApplicationContext(), R.string.message_max_vol_mode, Toast.LENGTH_LONG).show();
                        }
                        catch(Exception e){
                            Log.e(APP_TAG, "Setting up receiver GUI FAILED");
                        }
                    }


                }

            });

            mWasSetWatchingBefore = true;
        }





        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                if(mIsBroadcaster){
                    try {
                        if(on){
                            getSwitchCameraButtton().setVisibility(View.VISIBLE);
                            getSwitchCameraButtton().startAnimation(getFadeInAnimation());
                        }
                        else{
                            getSwitchCameraButtton().startAnimation(getFadeOutQuickAnimation());
                            //getSwitchCameraButtton().setVisibility(View.INVISIBLE);
                        }

                    }
                    catch(Exception e){
                        Log.e(APP_TAG, "Setting up broadcaster GUI FAILED");
                    }
                }
                else{
                    try{
                        if(on){
                            getTurnVideoButtton().setVisibility(View.VISIBLE);
                            getTurnVideoButtton().startAnimation(getFadeInAnimation());
                        }
                        else{
                            getTurnVideoButtton().startAnimation(getFadeOutQuickAnimation());
                            //getTurnVideoButtton().setVisibility(View.INVISIBLE);
                        }

                    }
                    catch(Exception e){
                        Log.e(APP_TAG, "Setting up receiver GUI FAILED");
                    }
                }

                setError(!on);
                if(on){

                }
                else{
                    getVideoTurnedOffImage().setVisibility(View.INVISIBLE);
                }


            }
        });
    }




    private ImageView getSplashScreenIcon() {
        ImageView view = (ImageView) findViewById(R.id.splash_screen_icon);
        return view;
    }

    private ImageView getSplashScreenBackground() {
        ImageView view = (ImageView) findViewById(R.id.splash_screen_background);
        return view;
    }

    private ProgressBar getLoading() {
        ProgressBar view = (ProgressBar) findViewById(R.id.loading);
        return view;
    }




    private ImageView getBabyDeviceImage() {
        ImageView image = (ImageView) findViewById(R.id.baby_device);
        return image;
    }



    private ImageView getParentDeviceImage() {
        ImageView image = (ImageView) findViewById(R.id.parent_device);
        return image;
    }


    private ImageButton getTurnVideoButtton() {
        ImageButton button = (ImageButton) findViewById(R.id.turn_video);
        return button;
    }

    private ImageButton getSwitchCameraButtton() {
        ImageButton button = (ImageButton) findViewById(R.id.switch_camera);
        return button;
    }




    private ImageView getErrorImage() {
        return (ImageView) findViewById(R.id.error_icon);
    }

    private TextView getErrorDescription() {
        return (TextView) findViewById(R.id.error_description);
    }

    private ImageView getVideoTurnedOffImage() {
        return (ImageView) findViewById(R.id.video_turned_off_icon);
    }


    private void playSoundForXSeconds(final Uri soundUri, int seconds) {
        if(soundUri!=null) {
            final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), soundUri);
            try {
                mp.start();
            }catch(Exception e) {
                Log.e(APP_TAG, "Cannot play sound for x seconds: " + e.getMessage());
            }

            mHandler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        mp.stop();
                    }catch(Exception e) {
                        Log.e(APP_TAG, "Cannot stop sound after x seconds: " + e.getMessage());
                    }
                }
            }, seconds * 1000);
        }
    }


    private void setError(boolean on){
        if(on){
            getErrorImage().setVisibility(View.VISIBLE);
            getErrorDescription().setVisibility(View.VISIBLE);

            if(!mIsBroadcaster){

                try {
                    playSoundForXSeconds(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE), 2);

                } catch (Exception e) {
                    Log.e(APP_TAG, "Playing disconnect sound warning FAILED: " + e.getMessage());
                }
            }
        }
        else{
            getErrorImage().setVisibility(View.INVISIBLE);
            getErrorDescription().setVisibility(View.INVISIBLE);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(APP_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        needAudioVideoPermission();

        preventAppBeTurnedOff();

        initWifiDirect();

        launchAudioVideoTask();
    }


    @Override
    protected void onStart() {
        Log.d(APP_TAG, "onStart");
        super.onStart();

    }

    @Override
    protected void onRestart() {
        Log.d(APP_TAG, "onRestart");
        super.onRestart();

        /*if(mIsBroadcaster){
            setSilentMode();
        }
        else{
            maximizeMediaVolume();
        }*/
    }

    @Override
    protected void onResume() {
        Log.d(APP_TAG, "onResume");
        super.onResume();

        registerReceiver(mWifiDirectReceiver, mIntentFilter);
        discoverPeers();
    }



    @Override
    protected void onPause() {
        Log.d(APP_TAG, "onPause");
        super.onPause();

        unregisterReceiver(mWifiDirectReceiver);
    }

    @Override
    protected void onStop() {
        Log.d(APP_TAG, "onStop");
        /*cancelSilentMode();
        unmaximizeMediaVolume();*/
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(APP_TAG, "onDestroy");
        cancelSilentMode();
        unmaximizeMediaVolume();
        unmaximizeVoiceCallVolume();

        allowAppBeTurnedOff();
        super.onDestroy();

        // release camera


    }




    private void startWiFiDirectMessaging(){

        Toast.makeText(getApplicationContext(), R.string.message_talking_to_other_device, Toast.LENGTH_LONG).show();
        launchProcessAsyncMessageTaskTimer();


        new SendOrReceiveMessageAsyncTask(mIsGroupOwner, mGroupOwnerAddressHost).execute();
        mIsSomehowConnected = true;
    }



    public void turnVideoClicked(View view){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getTurnVideoButtton().setEnabled(false);
                getLoading().setVisibility(View.VISIBLE);

            }
        });

        turnVideo(!isVideoTurnedOn());
    }


    boolean isVideoTurnedOn(){
        return getVideoTurnedOffImage().getVisibility() != View.VISIBLE;
    }




    public void switchCameraClicked(View view){
        switchCamera();
    }



    //endregion GUI


}
